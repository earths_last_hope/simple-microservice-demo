from django.db import models

class Movie(models.Model):
    title: models.CharField(max_length=60)
    description: models.TextField()
    year: models.SmallIntegerField()

    def __str__(self):
        return self.title